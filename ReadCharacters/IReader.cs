﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace ReadCharacters
{
    public interface IReader
    {

        void ReadCharsFromSource(string pathToSource);
        List<Character> GetCharacters();

    }
}
