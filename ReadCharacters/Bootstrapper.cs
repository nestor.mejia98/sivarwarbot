﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;

namespace ReadCharacters
{
    public class Bootstrapper : Registry
    {

        public Bootstrapper() {

            For<IReader>().Use<TxtReader>();

        }

    }
}
