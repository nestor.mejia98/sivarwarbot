﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace ReadCharacters
{
    public class TxtReader : IReader
    {

        private List<Character> Characters;

        public TxtReader() {

            Characters = new List<Character>();

        }

        public List<Character> GetCharacters()
        {
            return Characters;
        }

        public void ReadCharsFromSource(string pathToSource)
        {
            

            if (System.IO.File.Exists(pathToSource)) {


                using (var objReader = new System.IO.StreamReader(pathToSource)) {

                    do
                    {


                        Characters.Add(new Character
                        {
                            Name = objReader.ReadLine(),
                            Kills = 0,
                            IsAlive = true
                        });

                    } while (objReader.Peek() != -1);

                }

    

            }

        }
    }
}
