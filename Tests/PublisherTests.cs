﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using CommunicateFight;

namespace Tests
{
    public class PublisherTests
    {

        [Test]
        public void CanPublishATextOnlyTweet() {

            IPublisher myPublisher = new TwitterPublisher();

            myPublisher.PublishUpdate("Second programatically sent tweet");

            Console.WriteLine("done");

        }

        [Test]
        public void CanPublishTextAndImageTweet() {

            IPublisher myPublisher = new TwitterPublisher();

            myPublisher.PublishUpdateWithImage("Tweet with bitmap image", @"C:\Users\nesto\source\repos\SivarWarbot\Media\layout1.jpeg");

            Console.WriteLine("done");

        }

    }
}
