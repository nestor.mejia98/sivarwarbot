﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using CharacterFight;
using Entities;

namespace Tests
{
    public class FightingTests
    {

        [Test]
        public void CanGenerateADuel() {

            var fight = new ExecuteFight();

            Tuple<Character, Character>  duelists = fight.PickDuel();

            Assert.AreEqual(2, 2);

        }

        [Test]
        public void CanPickAWinnerFromDuel() {

            var duel = new Fight(new Character { Name = "Hola", IsAlive = true, Kills =1}, new Character { Name="Adiós", IsAlive = true, Kills=2});

            var winner = duel.GetWinner();

            Assert.AreEqual("Entities.Character", winner.GetType().ToString());

        }

        [Test]
        public void CanDetermineWinnerAndLoserAndUpdateList() {

            var fight = new ExecuteFight();

            fight.Fight();

        }

        [Test]
        public void CanSupportTwentieIterations() {

            var fight = new ExecuteFight();

            fight.Fight();

            for (var i = 0; i < 20; i++) {

                fight.Fight();

            }

            Console.Write("");

        }

    }
}
