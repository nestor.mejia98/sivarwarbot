﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ReadCharacters;

namespace Tests
{
    public class ReaderTests
    {

        [Test]
        public void CanReadCharactersFromTxt() {

            var txtReader = new TxtReader();

            txtReader.ReadCharsFromSource(@"C:\Users\nesto\source\repos\SivarWarbot\Characters.txt");

        }

        [Test]
        public void CanGetCharacters() {

            var txtReader = new TxtReader();

            txtReader.ReadCharsFromSource(@"C:\Users\nesto\source\repos\SivarWarbot\Characters.txt");

            Assert.AreEqual(20, txtReader.GetCharacters().Count);

        }

    }
}
