﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using BuildImage;
using Entities;

namespace Tests
{
    public class ImageBuilderTests
    {

        [Test]
        public void CanGenerateImageFromList() {

            var file = ImageBuilder.GetImageFromList(new List<Character> { new Character { Name = "Nestor", Kills = 1, IsAlive=true}, new Character { Name = "Ulises", Kills = 0, IsAlive = false } });

            Console.WriteLine("Done");

        }

    }
}
