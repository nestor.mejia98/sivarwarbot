﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicateFight
{
    public interface IPublisher
    {

        string GetImageToAttach(string ImagePath);
        void PublishUpdate(string text);
        void PublishUpdateWithImage(string text, string ImagePath);

    }
}
