﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace BuildImage
{
    public static class ImageBuilder
    {

        public static string GetImageFromList(List<Character> list) {

            PointF firstLocation = new PointF(10f, 10f);
            PointF secondLocation = new PointF(10f, 50f);

            string imageFilePath = getLayoutPath();
            Bitmap bitMapImage = new Bitmap(imageFilePath);//load the image file

            Graphics graphicImage = Graphics.FromImage(bitMapImage);
            graphicImage.SmoothingMode = SmoothingMode.AntiAlias;

            int x = 50;
            int y = 50;
            Brush textColor;
            for (int i = 0; i<list.Count; i++) {

                if (((i + 1) % 20)==0) {
                    y = 50;
                    x += 400;
                }

                if (!list[i].IsAlive)
                {
                    textColor = Brushes.Red;
                }
                else {
                    textColor = Brushes.Black;
                }

                graphicImage.DrawString(list[i].Name+" ("+list[i].Kills+")", new Font("Arial", 16, FontStyle.Bold), textColor, new Point(x, y));
                y += 25;

            }

            var newFile = InsertDateToFile(imageFilePath);
            //Save the new image to the response output stream.
            bitMapImage.Save(newFile, ImageFormat.Jpeg);


            graphicImage.Dispose();
            bitMapImage.Dispose();

            return newFile;

        }

        private static string getLayoutPath() {

            var baseDir = AppContext.BaseDirectory.Split(new string[] { "SivarWarbot" }, StringSplitOptions.None);

            string mediaPath = baseDir[0] + "SivarWarbot\\Media\\layout.bmp";


            return mediaPath;

        }

        private static string InsertDateToFile(string filename) {

            string[] path = filename.Split(new char[] { '.' });


            string formattedDate = DateTime.Now.GetHashCode().ToString();


            var filenameWithDate = path[0] + formattedDate + ".jpeg";


            return filenameWithDate;

        }

    }
}
