﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace CharacterFight
{
    public class Fight
    {
        private Character character1, character2;
        private Random HighestNumber;

        public Fight(Character c1, Character c2) {

            character1 = c1;
            character2 = c2;
            HighestNumber = new Random();

        }

        public Character GetWinner() {

            Double perc1 = HighestNumber.NextDouble();
            Double perc2 = HighestNumber.NextDouble();

            if (perc1 > perc2)
            {
                Console.WriteLine();
                return character1;
            }
            else if (perc2 > perc1)
            {
                return character2;
            }
            else {
                return GetWinner();
            }

        }

    }
}
