﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;
using ReadCharacters;
using Entities;
using BuildImage;
using CommunicateFight;

namespace CharacterFight
{
    public class ExecuteFight
    {

        public Container container = new Container(c => { c.AddRegistry<Bootstrapper>(); });
        public IReader reader;
        private List<Character> DeadAndAliveChars;
        private Random r;
        private int AliveChars;
        private TwitterPublisher tw;
        

        public ExecuteFight() {
            reader = container.GetInstance<IReader>();
            reader.ReadCharsFromSource(GetMediaPath());
            DeadAndAliveChars = reader.GetCharacters();
            ImageBuilder.GetImageFromList(DeadAndAliveChars);
            r = new Random();
            tw = new TwitterPublisher();
            AliveChars = DeadAndAliveChars.Count;
        }

        //C:\Users\nesto\source\repos\SivarWarbot\ConsoleFighting\bin\Debug\
        private string GetMediaPath() {

            string[] solutionPath = AppContext.BaseDirectory.Split(new string[] { "ConsoleFighting"}, StringSplitOptions.None);

            string mediaPath = solutionPath[0] + "Characters.txt";

            return mediaPath;

        }

        public Tuple<Character, Character> PickDuel() {

            var contenders = GetAliveChars();

            int firstRandom = r.Next(0, contenders.Count);

            int secondRandom = AvoidEqualRandoms(firstRandom, contenders.Count);

            var duelists = Tuple.Create(contenders[firstRandom], contenders[secondRandom]);

            return duelists;

        }

        private int AvoidEqualRandoms(int first, int upperCap) {
            var sRandom = r.Next(0, upperCap);
            if (sRandom == first) {

                sRandom = AvoidEqualRandoms(first, upperCap);

            }

            return sRandom;

        }

        private List<Character> GetAliveChars() {

            var aliveChars = new List<Character>();

            foreach (Character c in DeadAndAliveChars) {

                if (c.IsAlive) {

                    aliveChars.Add(c);

                }

            }

            return aliveChars;

        }

        public void Fight() {

            if (AliveChars >= 2) {

                var duelists = PickDuel();

                var fight = new Fight(duelists.Item1, duelists.Item2);

                var winner = fight.GetWinner();

                Character loser;

                if (winner.Equals(duelists.Item1))
                {
                    loser = duelists.Item2;
                }
                else
                {
                    loser = duelists.Item1;
                }

                string outcome = UpdateList(winner, loser);

                ImageBuilder.GetImageFromList(DeadAndAliveChars);

                //tw.PublishUpdateWithImage(outcome, ImageBuilder.GetImageFromList(DeadAndAliveChars));

            }

            

        }

        private string UpdateList(Character winner, Character loser) {

            foreach (Character c in DeadAndAliveChars) {

                if (c.Name.Equals(winner.Name)) {
                    c.Kills += 1;
                }

                if (c.Name.Equals(loser.Name)) {
                    c.IsAlive = false;
                }

            }

            AliveChars--;

            string fightOutcome = winner.Name + " (" + winner.Kills + ") ha asesinado a " + loser.Name + " (" + loser.Kills + "). Quedan " + AliveChars + " personajes. #SivarWarbot";

            Console.WriteLine(fightOutcome);

            return fightOutcome;

        }

    }
}
