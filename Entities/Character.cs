﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Character
    {

        public string Name { get; set; }
        public int Kills { get; set; }
        public bool IsAlive { get; set; }

        public string GetCharacterInfo() {

            return Name +" (" + Kills + ")";

        }

    }
}
